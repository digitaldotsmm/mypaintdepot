<?php get_header(); $current = get_queried_object(); ?>

<div id="brand-list">

	<div id="archivecatalog" >
		<div class="banner text-center">
			<div class="bannertitle">
				<h1 class="contactbtmline"><?php echo $current->name; ?></h1>
				<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
			</div>		
		</div>
		<div class="container">

			<div class="content-wrapper clearfix margintop40 marginbottom40"> 

				<?php
				$object = get_queried_object(); 

				$term_id = $object->term_id;

			 	$sub_terms = get_terms( array(
			            'taxonomy'      => MPD_PRODUCT_BRAND_TYPE,
			            'hide_empty'    => false,
			            'parent' => $term_id
			        ) ); 

					if (!empty($sub_terms)) { ?>

					<div class="row">

					<?php	foreach ($sub_terms as $list) {

						$image = get_field('category_image', MPD_PRODUCT_BRAND_TYPE . "_" . $list->term_id);
						$brand_img = aq_resize($image, 580, 300, true, true, true);
						$brand_name = $list->name; 
						$brand_link = get_term_link($list->slug, MPD_PRODUCT_BRAND_TYPE); 
					?>
					
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<div class="portfolio">
							<figure>
								<img src="<?php echo $brand_img; ?>" alt="<?php echo $brand_name; ?>" title="<?php echo $brand_name; ?>">
								<h3 class="brand-title">
									<a href="<?php echo $brand_link; ?>"><?php echo $brand_name; ?></a>
								</h3>
								<figcaption>
									<div class="fig-content-wrapper"> 
										<div class="fig-content">
											
											<div class="fig-overlay">
												<a class="zoom" href="<?php echo $image; ?>" rel="prettyPhoto" title="<?php echo $brand_name; ?>"> <span class="fa fa-search-plus"> </span> </a>
												<a class="link" href="<?php echo $brand_link; ?>"> <span class="fa fa-arrow-right"> </span> </a>
											</div>

										</div>
									</div>  
								</figcaption>
							</figure>
						</div>
					</div>
					
				<?php } ?>
				</div>
				<?php
				}else { 
				?>		

				<?php 
					$x = 1;
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
				
						$postimg_url = get_the_post_thumbnail_url($post->ID);
						//$small_img = aq_resize($postimg_url,180,180,true,true,true); 

						$link = get_permalink();
						
						if ( ($x == 1) || ($x % 4 == 1) ) {
							echo '<div class="row marginbottom30">';
						} 
				?>
				<div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 prodmbile">
					<div class="catawrapper">
						
						<a class="full-link" href="<?php echo get_permalink($post->ID); ?>">
							<img class="img-responsive" src="<?php echo $postimg_url; ?>" alt="<?php echo $post->post_title; ?>">
						</a>
						<div class="itemhover">
							<div><?php echo $post->post_title; ?></div>
						</div>

					</div>
					<div class="falldown">
						<a href="<?php echo get_permalink($post->ID); ?>" id="vdetailbtn" data-hover="View Detail">View Detail</a>
					</div>
				</div>

				<?php if ($x % 4 == 0) { 
				echo '</div>';
				} $x++; ?>

				<?php endwhile; endif; ?>

				<?php
				$count = $wp_query->max_num_pages;
				
				if($count > 1){
				?>
				<div class="col-md-12 col-sm-12 col-xs-12 text-center">
				    <div id="paging-link">
				    <?php

		              $big = 999999999; // need an unlikely integer

		              echo paginate_links( array(
		                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		                'format' => '?paged=%#%',
		                'current' => max( 1, get_query_var('paged') ),
		                'total' => $wp_query->max_num_pages
		              ) );

				    ?>
				    </div>
				</div>
				<?php } ?>

				<?php } ?>
				</div>
				
			</div>

		</div>

	</div>

</div>

<?php get_footer(); ?>




