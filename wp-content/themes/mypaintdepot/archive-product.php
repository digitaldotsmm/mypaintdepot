<?php get_header(); ?>

<div id="archiveproduct" >  
	<div class="banner text-center" style="min-height:160px;background: linear-gradient(to right, #00d39f 0%, #009ece 25%, #156396 75%, #2c3e50 100%);postion:relative;"  >
		<div class="bannertitle">
			<h1 class="contactbtmline">Products</h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
		</div>
		
	</div>
	<div class="container"> 
		<div class="content-wrapper clearfix margintop40 marginbottom40">
			<div class="row">
				<?php if(have_posts()): while(have_posts()): the_post(); ?>
					<?php 
						$product_image = get_the_post_thumbnail_url($post->ID);
	                    //$product_image = aq_resize($img_url, 180, 180, true, true, true);
	                    $price = get_field('product_price',$post->ID);
	                    $product_name = $post->post_title;
	                    
					?>
					<div class="col-xs-12 col-sm-6 col-lg-3 col-md-3 marginbottom20">
						<div class="catawrapper">
							<a class="full-link" href="<?php echo get_permalink($post->ID); ?>">
								<img class="img-responsive" src="<?php echo $product_image; ?>" alt="<?php echo $product_name; ?>">
							</a>
							<div class="itemhover">
								<?php if(!empty($price)){ ?>
									<span class="itemprice"><?php echo $price; ?></span>
								<?php } ?>
								<div><?php echo $product_name; ?></div>
							</div>
						</div>
						<div class="falldown">
							<a href="<?php echo get_permalink($post->ID); ?>" id="vdetailbtn" data-hover="View Detail">View Detail</a>
						</div>
					</div>			
				<?php endwhile; endif; ?>
				<?php
				$count = $wp_query->max_num_pages;
				
				if($count > 1){
				?>
				<div class="col-md-12 col-sm-12 col-xs-12 text-center">
				    <div id="paging-link">
				    <?php

		              $big = 999999999; // need an unlikely integer

		              echo paginate_links( array(
		                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		                'format' => '?paged=%#%',
		                'current' => max( 1, get_query_var('paged') ),
		                'total' => $wp_query->max_num_pages
		              ) );

				    ?>
				    </div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
	<?php get_footer(); ?>
