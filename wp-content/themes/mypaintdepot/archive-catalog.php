<?php get_header(); $current = get_queried_object();?>

<div id="archivecatalog" >
	<div class="banner text-center">
		<div class="bannertitle">
			<h1 class="contactbtmline"><?php echo $current->name; ?></h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
		</div>
	</div>
	<div class="container">
		<div class="clearfix margintop40 marginbottom40">

			<?php 
				$x = 1;
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
			
					$postimg_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'full');
					$small_img = aq_resize($postimg_url,184,266,true,true,true); 

					$link = get_permalink();
					if ($x == 1) {
						echo '<div class="row">';
					} elseif ($x % 4 == 1) {
						echo '<div class="row">';
					} 
			?>
			<div class="col-xs-12 col-sm-6 col-lg-3 col-md-3 text-center">
				<div class="catalogwrap">
					<img class="" src="<?php echo $small_img; ?>" alt="...">
					<div class="ctitle"><a href="<?php echo $link; ?>"><?php echo $post->post_title; ?></a></div>
				</div>
			</div>
			<?php if ($x % 4 == 0) { ?> 
				</div>
			<?php } $x++; ?>
			<?php endwhile;endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>