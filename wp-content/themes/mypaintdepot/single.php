<?php get_header(); ?>

<div id="archivecatalog" >
	<div class="banner text-center" style="min-height:160px;background: linear-gradient(to right, #00d39f 0%, #009ece 25%, #156396 75%, #2c3e50 100%);postion:relative;"  >
		<div class="bannertitle">
			<h1 class="contactbtmline">Catalogs</h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
		</div>
		
	</div>
	<div class="container">
		<div class="clearfix margintop40 marginbottom40">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2><?php the_title(); ?></h2>
					<div class="text-left">
						<?php echo apply_filters("the_content",$post->post_content); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
