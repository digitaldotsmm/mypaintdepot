<?php /* Template Name: Service */ ?>
<?php get_header(); ?>
<div id="servicepage">
	<div class="banner text-center">
		<div class="bannertitle">
			<h1 class="contactbtmline"><?php the_title(); ?></h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="serviceinfowrap">
					<?php echo apply_filters("the_content",$post->post_content); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
