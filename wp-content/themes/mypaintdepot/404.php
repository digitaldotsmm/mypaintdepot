<?php get_header(); ?>

<div id="page" >
	<div class="banner text-center" >
		<div class="bannertitle">
			<h1 class="contactbtmline">Page Not Found</h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<img src="<?php echo ASSET_URL; ?>images/404.jpg">
			</div>
			<div class="col-md-12 text-center marginbottom30">
				<p style="font-size:20px;">Opp!...you have requested the page that is no longer there.</p>
				<a href="/" title="Back to site" class="" style="background:#000;padding:10px 15px;color:#fff;">Go To Home</a>
			</div>
			
		</div>
	</div>
</div>
<?php get_footer(); ?>