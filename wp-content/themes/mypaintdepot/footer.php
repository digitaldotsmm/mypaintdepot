</div><!-- #content -->
</div><!-- #main -->
<?php global $THEME_OPTIONS; ?>
<div class="cleared"></div>

<div id="footer">
	<?php if (!is_front_page()): ?>
		<div style="background:linear-gradient(to right, #00d39f 0%, #009ece 25%, #156396 75%, #2c3e50 100%);height:50px;position:relative;">
				<div id="hehehe">
				</div>
		</div>
	<?php endif ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">

				<div class="addrwrap">
					<?php if ($THEME_OPTIONS[info_phone]): ?>
						<div class="marginbottom10"><?php echo $THEME_OPTIONS[info_phone];?></div>
					<?php endif ?>
					<?php if ($THEME_OPTIONS[info_address]): ?>
						<div><?php echo $THEME_OPTIONS[info_address];?></div>
					<?php endif ?>
				</div>
				<div class="socialwrap">
					<?php if( $THEME_OPTIONS['facebookid'] ): ?>
		            <a target="_blank" href="<?php echo $THEME_OPTIONS['facebookid']; ?>" title="follow us on Facebook"><i class="fa fa-facebook facebook" aria-hidden="true"></i></a>
		            <?php endif; ?>
		            <?php if( $THEME_OPTIONS['twitterid'] ): ?>
		            <a target="_blank" href="<?php echo $THEME_OPTIONS['twitterid']; ?>" title="follow us on Twitter"> <i class="fa fa-twitter twitter" aria-hidden="true"></i></a>
		            <?php endif; ?>
				</div>
				<div class="copywrap">
					Copyright &COPY; <?php echo date('Y'); ?> My paint Depot<br> Developed by <a title="Digital Dots, Yangon's web development company" target="_blank" href="http://www.digitaldots.com.mm">Digital Dots</a>
				</div>
			</div>
		</div>
	</div>

</div>				
<?php wp_footer(); ?>
</body>
</html>