<?php /* Template Name: Contact us */ ?>
<?php get_header() ?>
<?php global $THEME_OPTIONS; ?>

<div id="contactus" >
	<div class="banner text-center">
		<div class="bannertitle">
			<h1 class="contactbtmline"><?php the_title(); ?></h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
				<div id="googleMap">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3818.648055891865!2d96.19837423158349!3d16.84380699680591!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30c193203bd5519f%3A0xaed662230890481f!2sYadanar+Rd%2C+Yangon!5e0!3m2!1sen!2smm!4v1509360822467" width="100%" height="760" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
				<div class="contactformwrap">
					<h2 class="tblock-title">SEND US A MESSAGE</h2>
					<?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]');?>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 text-center">
							<?php if ($THEME_OPTIONS[info_phone]): ?>
								<i class="fa fa-mobile fa-2x" aria-hidden="true"></i>
								<h4>call us</h4>
								<p><?php echo $THEME_OPTIONS['info_phone']; ?></p>
							<?php endif ?>
						</div>
						<div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 text-center">
							<?php if ($THEME_OPTIONS[info_email]): ?>
								<i class="fa fa-pencil-square-o fa-2x"></i>
								<h4>contact us</h4>
								<p><a href="mailto:<?php echo $THEME_OPTIONS['info_email']; ?> ?>"><?php echo $THEME_OPTIONS['info_email']; ?></a></p>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>