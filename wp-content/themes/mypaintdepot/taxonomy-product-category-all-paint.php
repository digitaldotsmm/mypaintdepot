<?php get_header(); $current = get_queried_object(); ?>

<div id="archiveproduct" >
	<div class="banner text-center">
		<div class="bannertitle">
			<h1 class="contactbtmline">Choose Your Paints Brand</h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
		</div>
	</div>
	<div class="container">
		<div class="row clearfix paddingtop40 paddingbottom40">
			<?php 
				$terms = get_terms( array(
						'taxonomy' => MPD_PRODUCT_BRAND_TYPE,
						'hide_empty' => false,
						'parent'   => 0,
					));
				$x = 1;
				foreach ($terms as $brand):

					$id = $brand->taxonomy.'_'.$brand->term_id;
					$category_img = get_field('category_image', $id );
					$small_img = aq_resize($category_img['url'],148,95,true,true,true); 
					
					if ($x == 1) {
						echo '<div class="row udline">';
					} elseif ($x % 4 == 1) {
						echo '<div class="row udline">';
					} 
			?>
			<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 brandlogowrap">
				<div class="icon-wrapper">
					<a href="/<?php echo $brand->taxonomy.'/'.$brand->slug; ?>/?p_type=product"><img class="" src="<?php echo $small_img; ?>" alt="<?php echo $brand->name; ?>"></a>
					<a class="eff003" href="/<?php echo $brand->taxonomy.'/'.$brand->slug; ?>/?p_type=product">View Detail<span class="fa fa-long-arrow-right marginleft10"></span></a>
				</div>
			</div>
			<?php if ($x % 4 == 0) { ?> 
				</div>
			<?php } $x++; ?>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
