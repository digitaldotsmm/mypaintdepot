<?php /* Template Name: Order */ ?>
<?php get_header(); ?>

<div id="page" >
	<div class="banner text-center">
		<div class="bannertitle">
			<h1 class="contactbtmline"><?php the_title(); ?></h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12 nopadding order-img"> 
			<?php
			if (has_post_thumbnail()) {
				$post_img = get_the_post_thumbnail_url($post->ID);
				$post_img = aq_resize($post_img, 585, 900, true, true, true);
			?>
			<img class="img-responsive" src="<?php echo $post_img; ?>" alt="<?php echo bloginfo('name'); ?>">
			<?php } ?>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 nopadding">
				<div class="contactformwrap">
					<h2 class="tblock-title">Inquiry Form</h2>
					<?php echo do_shortcode('[contact-form-7 id="263" title="Inquiry Form"]'); ?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
