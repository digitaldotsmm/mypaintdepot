/*!
 *      Author: DigitalDots
	name: script.js	
	requires: jquery	
 */

var ddFn = {
    
    init: function(){
        Global = new this.Global();
        Utils = new this.Utils();     
        Tour = new this.Tour();   
        
        Global.__init();        
    },
    

    Global:function(){        
        
        this.__init = function(){ 
            
            $("a[rel^='prettyPhoto']").prettyPhoto({
               show_title: false,
               social_tools:false
            });

            $(".soliloquy-image").after('<img src="/wp-content/themes/mypaintdepot/assets/images/curve_top.png" class="imgcurve img-responsive">')

            // hover on item 
            $( ".itemwrap" ).hover(function() {
                $(this).addClass("pulse");
            }, function() {
                $( this ).removeClass( "pulse" );
                $( this ).removeClass( "bigEntrance" );
            });                

            $( ".catawrapper" ).hover(function() {
                $(this).addClass("pulse");
            }, function() {
                $( this ).removeClass( "pulse" );
                $( this ).removeClass( "bigEntrance" );
            });                
        }
      
        this.home_url = function(){
            var home_page_url = window.location.protocol + "//" + window.location.host + "/";     
            return home_page_url;
        }
        this.template_url = function(){
            return this.home_url() + 'wp-content/themes/mypaintdepot/';           
        }        
        this.ajax_url = function(){
            var ajax_url = this.home_url() + "wp-admin/admin-ajax.php";        
            return ajax_url;
        }              
    }, // end of Global

     Tour:function(){        
        
        this.__init = function(){   
         }
      
        this.home_url = function(){
            var home_page_url = window.location.protocol + "//" + window.location.host + "/";     
            return home_page_url;
        }
        this.template_url = function(){
            return this.home_url() + 'wp-content/themes/mypaintdepot/';           
        }        
        this.ajax_url = function(){
            var ajax_url = this.home_url() + "wp-admin/admin-ajax.php";        
            return ajax_url;
        }              
    }, // end of Global

    Utils: function(){
        //this.exist= false;
        this.validate_email = function (email){	
            var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            //var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
            if(filter.test(email)){
                return true;
            }
            else{
                return false;
            }
        };	
	
        this.format_my_date = function(dateStr) {
            return dateStr.replace(/^([a-z]{3})( [a-z]{3} \d\d?)(.*)( \d{4})$/i, '$1,$2$4$3');
        };
	
        this.remove_GMTorUTC = function(dateStr) {
            var noGMT = dateStr.replace(/^(.*)(:\d\d GMT+)(.*)$/i, '$1');  // Trim string like 'Thu Apr 21 2011 14:08:46 GMT+1000 (AUS Eastern Standard Time)'
            var noUTC = noGMT.replace(/^(.*)(:\d\d UTC+)(.*)$/i, '$1');    // Trim string like 'Thu Apr 21 14:08:46 UTC+1000 2011'
            return noUTC;
        };
        
        this.is_user_email_exist = function(email, callback){                 
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_email_exist',
                    'email'  :   email     
                },
                async: false,
                dataType:"json",
                success:function(response){                          
                    if(response.is_exist){
                        return callback (true);
                    }else{
                        return callback (false);
                    }
                }
            });            
        }; 
        this.is_user_name_exist = function(username, callback){              
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_name_exist',
                    'username'  :   username     
                },
                async: false,
                dataType:"json",
                success:function(response){                          
                    if(response.is_exist){                          
                        return callback (true);
                    }else{
                        return callback(false);
                    }
                }
            });            
        };
        this.is_user_pass_valid = function(username, userpass, callback){           
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data:  {
                    action : "is_password_exist", 
                    'id'   : username,
                    'pass' : userpass
                }, 
                async: false,
                dataType: "json",
                success:function(response){                          
                    if(response){                                 
                        return callback (true);
                    }else{                         
                        return callback(false);
                    }
                }
            });            
        };
        this.is_user_logged_in = function(callback){
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_logged_in'                     
                },
                async: false,
                dataType:"json",
                success:function(response){                       
                                               
                    return callback (response.is_logged_in);
                     
                }
            });      
        }                  
    }

};


var Global;
var Utils;
var Tour;

$ = $.noConflict();
$(document).ready(function(){  
    ddFn.init();
});