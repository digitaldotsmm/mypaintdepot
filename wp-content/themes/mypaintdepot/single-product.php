<?php get_header(); ?>

<div id="archivecatalog">
	<div class="banner text-center" style="min-height:160px;background: linear-gradient(to right, #00d39f 0%, #009ece 25%, #156396 75%, #2c3e50 100%);postion:relative;"  >
		<div class="bannertitle">
			<h1 class="contactbtmline">Product</h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();} ?></div>
		</div>
		
	</div>
	<div class="container">
		<div class="clearfix margintop40 marginbottom40">
			<?php 
				$product_img = get_the_post_thumbnail_url($post->ID);
				//$small_img = aq_resize($postimg_url,316,226,true,true,true);
				
				$product_title = $post->post_title;
				$price = get_field("product_price",$post->ID); 
				$product_type = get_field("product_type",$post->ID); 
				$area = get_field("area_of_use",$post->ID);
				$benefit = get_field("benefits",$post->ID);
				$benefits_myanmar = get_field("benefits_myanmar",$post->ID);
				$gallery = get_field("gallery",$post->ID);
				
			?>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="sg_img">
						<img class="product-img" src="<?php echo $product_img; ?>" alt="<?php echo $product_title; ?>" title="<?php echo $product_title; ?>">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-8 col-md-8">
					<div class="detail-box">
						<h2><?php echo $product_title; ?></h2>
						<?php if(!empty($price)){ ?>
						<span class="product-price">Price: <?php echo $price; ?></span>
						<?php } if (!empty($product_type)) { ?>
						<span class="product-price"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Product Type: <?php echo $product_type; ?></span>
						<?php } if (!empty($area)) { ?>
						<span class="product-price"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Area of use: <?php echo $area; ?></span>
						<?php } ?>
						<div class="summary-box">
							<?php echo apply_filters("the_content",$post->post_content); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<?php if (!empty($benefit)) { ?>
						<div class="benefit-box">
						<h4 class="benefit-title"><i class="fa fa-home" aria-hidden="true"></i> Product Features</h4>
						<?php echo $benefit; ?>
						</div>
					<?php } ?>
					<?php if (!empty($benefits_myanmar)) { ?>
					<div id="myanmar-box">
						<div class="benefit-box">
						<h4 class="myanmar-title">ေဆးအသံုးျပုျခင္း အက်ိဳးေက်းဇူးမ်ား</h4>
						<?php echo $benefits_myanmar; ?>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6 col-md-6">
					<?php if (!empty($gallery)) {
					?>
					<div id="gallery-slider" class="carousel slide" data-ride="carousel"> 
									
						<div class="carousel-inner">
						<?php 
						foreach ($gallery as $key=>$g) {
						$gallery_img = aq_resize($g['url'],600,400,true,true,true); 
						?>
						<div class="item <?php echo ($key==0)?'active':''; ?>"> 
						<img class="img-responsive" src="<?php echo $gallery_img; ?>" alt="<?php echo $product_title; ?>">
						</div>
						<?php } ?>
						</div>
						<ol class="carousel-indicators">
						<?php foreach ($gallery as $key1=>$g) { ?>
							<li data-target="#gallery-slider" data-slide-to="<?php echo $key1; ?>" class="<?php echo ($key1==0)?'active':''; ?>"></li>
						<?php } ?>
						</ol>			

					</div>
					<?php } ?>
				</div> 
			</div>
			
		</div>
	</div>
</div>
<?php get_footer(); ?>
