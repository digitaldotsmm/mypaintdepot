<?php /* Template Name: Catalogs */ ?>
<?php get_header(); ?>

<div id="archiveproduct" >
	<div class="banner text-center">
		<div class="bannertitle">
			<h1 class="contactbtmline"><?php the_title(); ?></h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
		</div>
	</div>
	<div class="container">
		<div class="col-md-12 clearfix paddingtop40 paddingbottom40" style="background:#fff;">
			<?php 
				$terms = get_terms( array(
						'taxonomy' => MPD_CATALOG_BRAND_TYPE,
						'hide_empty' => false,
						'parent'   => 0,
					));

					$x = 1;
					foreach ($terms as $brand):
						
					$image = get_field('category_image', MPD_CATALOG_TYPE . "_" . $brand->term_id);
					$small_img = aq_resize($image,148,95,true,true,true); 
					
					if ($x == 1) {
						echo '<div class="row udline">';
					} elseif ($x % 4 == 1) {
						echo '<div class="row udline">';
					} 
			?>
				<div class="col-xs-6 col-sm-3 col-lg-3 col-md-3 brandlogowrap">
					<div class="icon-wrapper">
					<a href="/<?php echo $brand->taxonomy.'/'.$brand->slug; ?>"><img class="" src="<?php echo $small_img; ?>" alt="..."></a>
					<a class="eff003" href="/<?php echo $brand->taxonomy.'/'.$brand->slug; ?>">View Detail<span class="fa fa-long-arrow-right marginleft10"></span></a>
					</div>
				</div>
			<?php if ($x % 4 == 0) { ?> 
				</div>
			<?php } $x++; ?>
			<?php endforeach ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
