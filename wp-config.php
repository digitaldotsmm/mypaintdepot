<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */
 switch ($_SERVER['HTTP_HOST']) {    
    case 'mypaintdepot.dd':    
        define('DB_NAME', 'mypaintdepot');
        define('DB_USER', 'webuser');
        define('DB_PASSWORD', 'nopassword');
        define('DB_HOST', '192.168.1.200');
        break;
    case 'mypaintdepot.myanmarcafe.info':
        define('DB_NAME', 'digitald_mypaintdepot');
        define('DB_USER', 'digitald_webuser');
        define('DB_PASSWORD', '9UrEbO{g0Ck3');
        define('DB_HOST', 'localhost');
        break;
    default :
        define('DB_NAME', '');
        define('DB_USER', '');
        define('DB_PASSWORD', '');
        define('DB_HOST', 'localhost');
        break;
}
$protocol = (!empty($_SERVER['HTTPS']) ) ? 'https://' : 'http://';
define('WP_SITEURL', $protocol . $_SERVER['HTTP_HOST']);
define('WP_HOME', $protocol . $_SERVER['HTTP_HOST']);
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3}Qf[8=9QUb>cl;mP!l(_O`E,;q`A!D`xwebF:`IAh?*%rgZ;H[j, G#ETyayc`X');
define('SECURE_AUTH_KEY',  ' 9W^7&DI`+QGP0^f+#[Lp8mGVSp2k|@%>|N?nQ40Dmu=W>IsXovkgGaZ^7T<>`eh');
define('LOGGED_IN_KEY',    'UByRbIFq)y|cG;gPs;&]`TJve?%e*6w?^,K/Z}gFv*#+&,+VW>}%ja>z%F&hF &`');
define('NONCE_KEY',        'OmKEQO}+z}Vf-Pj%Jk}<xG,|6cXc)(+Z3JH7-0sMILa|e1x__ 6DO%W#dtfK6c->');
define('AUTH_SALT',        'c/e!a?Ow,Y+,@JK3jg4gB-}|*enFu6k 5-V6LZWk]ziyt5X07T4,w?-x}g ?DTyk');
define('SECURE_AUTH_SALT', '|G@pKZ7TCe/vw^fv-Is$-HpvE275oGJOj6K)LK`vW)<YGRW$Q]P*LQOK :@GZMeY');
define('LOGGED_IN_SALT',   '(9-4fxDF~1PED-#&o;Cdx6&&U88?|YVZ%9wDbB;Q0cT7#XqJKO|0jv^I=aTZ-Z`T');
define('NONCE_SALT',       'xxM?Y+.tVxG}iD64K$e1>d@{]ERwUk$D-G@ma6rN!1{#-3 yA27{Nrb;Kz3~WlWV');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */

define('AUTOSAVE_INTERVAL', 180);  
define( 'WP_AUTO_UPDATE_CORE', false);
define( 'WP_POST_REVISIONS', 3 );
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
