<?php /* Template Name: Brands */ ?>
<?php get_header(); ?>

<div id="brand-list">
	<div id="archiveproduct" >
		<div class="banner text-center">
			<div class="bannertitle">
				<h1 class="contactbtmline"><?php the_title(); ?></h1>
				<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
			</div>
		</div>
		<div class="container">
			<div class="content-box">
				<div class="row">
				<?php
				$brand_list = get_terms(MPD_PRODUCT_BRAND_TYPE,
					array(
					'hide_empty'=>0,
					'orderby'=>'term_id',
					'parent'=>0
					)
				);
				
				if(!empty($brand_list)){
					foreach ($brand_list as $list) {

					$image = get_field('category_image', MPD_PRODUCT_BRAND_TYPE . "_" . $list->term_id);
					$brand_img = aq_resize($image, 380, 250, true, true, true);
					$brand_name = $list->name; 
					$brand_link = get_term_link($list->slug, MPD_PRODUCT_BRAND_TYPE); 
				?>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="portfolio isotope-item">
						<figure>
							<img src="<?php echo $brand_img; ?>" alt="<?php echo $brand_name; ?>" title="<?php echo $brand_name; ?>">
							<figcaption>
								<div class="fig-content-wrapper"> 
									<div class="fig-content">
										
										<div class="fig-overlay">
											<a class="zoom" href="<?php echo $brand_img; ?>" rel="prettyPhoto" title="<?php echo $brand_name; ?>"> <span class="fa fa-search-plus"> </span> </a>
											<a class="link" href="<?php echo $brand_link; ?>"> <span class="fa fa-arrow-right"> </span> </a>
										</div>

									</div>
								</div>  
							</figcaption>
						</figure>
					</div>
				</div>
				<?php
					}
				}
				?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
