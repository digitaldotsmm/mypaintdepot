<?php get_header(); ?>
<div id="welcomemsg">
	<?php echo do_shortcode('[soliloquy id="156"]');?>
	<?php $welcomemsg = get_fields(MPD_PAGE_HOME); ?>
		<div class="welcomemsgwrap">
			<h1 class="welcomemsgtitle"><?php echo $welcomemsg['welcome_title']; ?></h1>
			<div><span><?php echo $welcomemsg['welcome_moto']; ?></span></div>
			<div class="margintop50">
				<a class="vproduct" href="/products-by-category/">View<br><span class="vp">Products</span></a>
				<a class="ordernow" href="/inquiry-form/">Order<br><span class="odn">Now!</span></a>
			</div>
		</div>
</div>	
<div id="welcome">
	<div class="container">
		<div class="row margintop60 marginbottom40">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"> 
				<h2 class="underline subtitle">Welcome to <span class="colorblue">"My Paint Depot"</span></h2>
	         	<?php 
	         		if( $about = get_page(MPD_PAGE_HOME) ) : 
	         		$aboutcontent = substr($about->post_content, 0, 600)."...";  
	         	?>
	         	<?php echo apply_filters("the_content",$aboutcontent); ?>
	         	
				<p><div class="readmore"><a href="<?php echo get_permalink(MPD_PAGE_ABOUT); ?>">Read More</a></div></p>
	         <?php endif; ?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<h2 class="underline subtitle">Our <span class="colorblue">Services</span></h2>

				<?php $serviceinfo = get_field('service',MPD_PAGE_SERVICE); ?>
				<?php 
					foreach ($serviceinfo as $service):
					$service_img = aq_resize($service['service_img']['url'],72,72,true,true,true); 
				?>
				<div class="service">
					<div class="service-left">
						<img class="" src="<?php echo $service_img; ?>" alt="My Paint Depot">
					</div>
					<div class="service-body">
						<?php echo $service['service_name']; ?>
					</div>	
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</div>
<div id="category">	
	<div class="container">	
		<div class="row margintop50 marginbottom50">	
			<div class="col-md-12 text-center">
				<h2 class="subtitle categorybtmline">Product By Category</h2>
			</div>
			<?php 	
				$productterms = get_terms( array(
				'taxonomy' => MPD_PRODUCT_CATEGORY_TYPE,
				'hide_empty' => false,
				'parent'   => 0,
				'exclude' =>48,
				));
			?>
			<?php 
					$x = 1;
					foreach ($productterms as $product_category):

					$img = get_field('category_image', MPD_PRODUCT_CATEGORY_TYPE . "_" . $product_category->term_id);
					$cate_img = aq_resize($img,300,300,true,true,true); 
			?>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<div class="categorywrap cteff01">

					<div class="zoomwrap"><a href="/<?php echo $product_category->taxonomy.'/'.$product_category->slug; ?>"><img class="img-responsive width100" src="<?php echo $cate_img; ?>" alt="<?php echo $product_category->name; ?>"><i class="fa fa-search" aria-hidden="true"></i></a></div>
					<a href="/<?php echo $product_category->taxonomy.'/'.$product_category->slug; ?>"><div class="catetitle"><?php echo $product_category->name; ?></div></a>

				</div>
			</div>

			<?php $x++;endforeach; ?>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center margintop40" >
				<div class="catereadmore"><a href="<?php echo get_permalink(MPD_PAGE_PRODUCT); ?>">Read More</a></div>
			</div>
		</div>
	</div>
</div>
<div id="bestitem">
	<div class="container">	
		<div class="row margintop60">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center marginbottom10">
				<h2 class="subtitle bitembtmline">BEST SELLING ITEMS</h2>
			</div>


			<?php 
				$args = array(
                            'posts_per_page' => 6,
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'post_type' => MPD_PRODUCT_TYPE, 
                            'meta_query' => array(
								array(
									'key' => 'best_selling_item',
									'value' => 'yes',
								))
                            );
				$count = 1;                        
            	$bestseling = get_posts($args);

            	foreach ($bestseling as $bestitem) {
            		$product_name = $bestitem->post_title;
            		$product_img = wp_get_attachment_image_src(get_post_thumbnail_id($bestitem->ID), 'full');
                    //$bestitemimg = aq_resize($img_url[0], 122, 122, true, true, true);
			?>
			

			<div class="col-xs-6 col-sm-4 col-md-2 col-lg-2 mb100 <?php if($count==2||$count==5){echo "itemtwo";}elseif($count ==3 || $count==4){echo "itemthree";} ?> ">
				<div class="itemwrap eff01">
					
					<a class="full-link" href="<?php echo get_permalink($bestitem->ID); ?>">
						<img class="img-responsive" src="<?php echo $product_img[0]; ?>" alt="<?php echo $product_name; ?>" title="<?php echo $product_name; ?>">
					</a>

					<div class="itemhover">
						<a href="<?php echo get_permalink($bestitem->ID); ?>"><div><?php echo $product_name; ?></div></a>
					</div>
				</div>
			</div>

			<?php $count++;} ?>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center margintop40" >
				<div class="itemreadmore"><a href="/product-category/decorative/">Read More</a></div>
			</div>
		</div>
	</div>
</div>
								
<div id="brand">
	<img src="<?php echo ASSET_URL; ?>images/curve_bottom.png" class="brandimgcurve img-responsive">

	<div class="container">
		<div class="row margintop70">
			<div class="brandwrap clearfix">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="tbrand">
						<h2>Best Selling<br> Brands</h2>
						<div class="brandreadmore"><a href="/brands">Read More</a></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<ul class="brandlogo">
						<?php 
							$brand_arg = array(
								'hide_empty' => false,
								'parent'   => 0,
								'number' => 6
							);
							$brandterms = get_terms(MPD_PRODUCT_BRAND_TYPE,$brand_arg);
							
							foreach ($brandterms as $brand):

								$brand_image = get_field('category_image', MPD_PRODUCT_BRAND_TYPE . "_" . $brand->term_id);
								$brand_image = aq_resize($brand_image,225,145,true,true,true);
								$brand_link = get_term_link($brand->slug, MPD_PRODUCT_BRAND_TYPE); 
						?>
						<li>
							<a href="<?php echo $brand_link; ?>">
								<img class="width100" src="<?php echo $brand_image; ?>" title="<?php echo $brand->name; ?>" alt="<?php echo $brand->name; ?>">
							</a>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>	
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){  
		console.log($( window ).width());
		if ($(window).width()>640) {
	        var welcomeoffset = $('#welcome').offset().top;
            var categoryofset = $('#category').offset().top;
            var bestitemofset = $('#bestitem').offset().top;
            var brandofset = $('#brand').offset().top;
        
            $(window).scroll(function() {
                var topOfWindow = $(window).scrollTop();

                if (topOfWindow > welcomeoffset) {
                    $('.cteff01').addClass("pullUp");
                }else if(welcomeoffset > topOfWindow){
                    $('.cteff01').removeClass("pullUp");
                }

                if (topOfWindow > categoryofset) {
                    $('.eff01').addClass("bigEntrance");
                }else if(categoryofset > topOfWindow){
                    $('.eff01').removeClass("bigEntrance");
                }

                if (topOfWindow > bestitemofset) {
                    $('.brandwrap').addClass("slideExpandUp");
                }else if(bestitemofset > topOfWindow){
                    $('.brandwrap').removeClass("slideExpandUp");
                }
            });
	}});
</script>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
