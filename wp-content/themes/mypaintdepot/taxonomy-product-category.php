<?php get_header(); $current = get_queried_object(); ?>

<div id="archiveproduct" >
	<div class="banner text-center">
		<div class="bannertitle">
			<h1 class="contactbtmline"><?php echo $current->name; ?></h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();} ?></div>
		</div>
	</div>
	<div class="container">
		<div class="row margintop40 marginbottom40">

			<?php 
				$brand_list = get_terms(MPD_PRODUCT_BRAND_TYPE, 
					array(
					'orderby' => 'term_id', 
					'hide_empty' => 0,
					'parent' => 0
						)
					);

				if(!empty($brand_list)) { ?> 

		      	<div class="col-md-12 col-sm-12 col-xs-12">
		      		<div class="filter-box">
			      		<h3><i class="fa fa-angle-double-left" aria-hidden="true"></i> <i class="fa fa-angle-double-left" aria-hidden="true"></i> <i class="fa fa-angle-double-left" aria-hidden="true"></i> View products by Brand <i class="fa fa-angle-double-right" aria-hidden="true"></i> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <i class="fa fa-angle-double-right" aria-hidden="true"></i></h3>
						<ul class="logo-list">
							<?php	
							foreach ($brand_list as $key => $type) { 
							$type_link = get_term_link($type->term_id,MPD_PRODUCT_BRAND_TYPE);
							?>
							<li><a href="<?php echo $type_link; ?>"><?php echo $type->name; ?></a></li>
							<?php } ?> 
						</ul>
					</div>
				</div>

			<?php } ?>

			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;				
				$args = array(
                    'posts_per_page' => 12,
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'post_type' => MPD_PRODUCT_TYPE, 
                    'paged' => $paged,
                    'tax_query' => array(
						array(
							'taxonomy' => $current->taxonomy,
							'field' => $current->slug,
							'terms' => $current->term_id, 
						))
                    );
                
                $catalog_data = get_posts($args);

                if(!empty($catalog_data)):

               	foreach ($catalog_data as $catalog):

               		$img_url = wp_get_attachment_image_src(get_post_thumbnail_id($catalog->ID), 'full');
                    //$catalog_image = aq_resize($img_url[0], 180, 180, true, true, true); 
                    //$catalogprice = get_field('product_price',$catalog->ID);
			?>		
			<div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 otherprod marginbottom20">
				<div class="catawrapper">
					<a class="full-link" href="<?php echo get_permalink($catalog->ID); ?>">
						<img class="img-responsive" src="<?php echo $img_url[0]; ?>" alt="<?php echo $catalog->post_title; ?>">
					</a>
					<div class="itemhover">
						<div><?php echo $catalog->post_title; ?></div>
					</div>
				</div>
				<div class="falldown">
					<a href="<?php echo get_permalink($catalog->ID); ?>" id="vdetailbtn" data-hover="View Detail">View Detail</a>
				</div>
			</div>			
			<?php endforeach; 
			endif; 
			?> 

			<?php
				$count = $wp_query->max_num_pages;
				
				if($count > 1){
				?>
				<div class="col-md-12 col-sm-12 col-xs-12 text-center"> 
				    <div id="paging-link">
				    <?php
				       echo paginate_links( array(
				         'format' => '?paged=%#%',
				         'current' => max( 1, get_query_var('paged') ),
				         'mid_size' => 3,
				         'total' => $wp_query->max_num_pages
				         
				       ) );
				    ?>
				    </div>
				</div>
			<?php } ?>

		</div>
	</div>
</div>
<?php get_footer(); ?>


		