<?php /* Template Name: Products by Category */ ?>
<?php get_header(); ?>

<div id="brand-list">

<div id="category-list">
	
	<div class="banner text-center">
		<div class="bannertitle">
			<h1 class="contactbtmline"><?php the_title(); ?></h1>
			<div class="breadcrumb"><?php if(function_exists('bcn_display')){bcn_display();}?></div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php 	
				$category_list = get_terms( array(
				'taxonomy' => MPD_PRODUCT_CATEGORY_TYPE,
				'hide_empty' => false,
				'parent'   => 0,
				));
				
			if (isset($category_list)) { 
			?>
			<div class="portfolio-container">
				<?php 
					foreach ($category_list as $category){

						$img = get_field('category_image', MPD_PRODUCT_CATEGORY_TYPE . "_" . $category->term_id);
						$category_img = aq_resize($img, 380, 600, true, true, true);
						$category_name = $category->name;
						$category_link = get_term_link($category->slug, MPD_PRODUCT_CATEGORY_TYPE); 
				?>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="portfolio">
						<figure>

							<a class="full-link" href="<?php echo $category_link; ?>">
							<img src="<?php echo $category_img; ?> " alt="<?php echo $category_name; ?>" title="<?php echo $category_name; ?>">
							</a>
							<h3 class="brand-title"><?php echo $category_name; ?></h3>

						</figure>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>
