<?php global $THEME_OPTIONS; ?>
<!doctype html>
<!--[if lt IE 7 ]>	<html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>		<html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>		<html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>		<html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en"  class="no-js">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<title><?php wp_title(''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<?php if ( file_exists(TEMPLATEPATH .'/favicon.png') ) : ?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png">
<?php endif; ?>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<?php $body_classes = join( ' ', get_body_class() ); ?>
<body class="<?php if( !is_search() ) echo $body_classes; ?>">


<div id="header">	
	<div class="container">
		<div class="row ">
			<div class="hidden-xs">
				<div class="col-xs-4 col-sm-4 col-md-5 col-lg-5">
					<nav class="margintop50" style="display: table;">
						<?php
							wp_nav_menu(array(
								'theme_location' => 'menu-left',
								'container_id' => 'menu-left',
								'menu_class' => 'nav navbar-nav',
								));
						?>   
					</nav>
				</div> 
			</div>
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
				<div class="logo aligncenter">
					<div class="identity">
						<a class="full-link" href="<?php echo site_url(); ?>">
							<?php 
								if($THEME_OPTIONS['logo']) {
									$logo_url=$THEME_OPTIONS['logo'];
								}
							?>
							<img src="<?php echo $logo_url; ?>" alt="<?php echo bloginfo('name'); ?>" title="<?php echo bloginfo('name'); ?>"> 
						</a>
					</div>
				</div>


				<div class="visible-xs">
					<!--  mobile menu  -->
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu" aria-expanded="false">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse" id="mobile-menu">
						<?php
							wp_nav_menu(array(
								'container' => 'div',
								'theme_location' => 'mobile-menu',
								'container_class' => 'mobile-menubar',
								'menu_class' => 'nav navbar-nav'
								));
						?>
					</div>
				</div>
			</div>
			<div class="hidden-xs">
			<div class="col-xs-4 col-sm-4 col-md-5 col-lg-5">
				<nav class="margintop50" style="display: table;">
					<?php
						wp_nav_menu(array(
							'theme_location' => 'menu-right',
							'container_id' => 'menu-right',
							'menu_class' => 'nav navbar-nav',                                
							));
					?>   
				</nav>
			</div>
			</div>
		</div>  
	</div>
</div><!--/header -->

<div id="main" class="clearfix">
	<div id="content">
